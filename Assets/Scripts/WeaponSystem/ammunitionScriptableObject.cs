using UnityEngine;

[CreateAssetMenu(fileName = "New Ammunition", menuName = "Ammunition")]
public class ammunitionScriptableObject : ScriptableObject
{
    public new string name;
    public string description;
    public string ammunitionType;

    public GameObject model;
}
