﻿using TMPro;
using UnityEngine;
using System.Collections.Generic;

public class CommandLine : MonoBehaviour {
    private const int MEMORY_SIZE = 10;

    private TMP_InputField commandInput;
    private int memoryIndex = -1;
    private List<string> commandMemory;

    private void Start() {
        commandInput = GetComponentInChildren<TMP_InputField>();
        commandMemory = new List<string>();

        // Add commands
        InitializeCommands();
    }

    private void InitializeCommands() {
        Commands.AddCommand("ping", 0, (args) => {
            Debug.Log("PONG");
        });

        Commands.AddCommand("exit", 0, (args) => {
            Application.Quit();
        });
    }

    public void SendCommand() {
        if (commandInput.text != "") {
            commandMemory.Insert(0, commandInput.text);
            if (commandMemory.Count > MEMORY_SIZE) commandMemory.RemoveAt(commandMemory.Count - 1);
            memoryIndex = -1;

            string[] divideString = commandInput.text.Split(' ');
            string command = divideString[0];
            string[] arguments = new string[0];
            if (divideString.Length > 1) {
                arguments = new string[divideString.Length - 1];
                for (int i = 1; i < divideString.Length; i++) {
                    arguments[i - 1] = divideString[i];
                }
            }

            if (arguments.Length == 0)
                Commands.ExecuteCommand(command);
            else
                Commands.ExecuteCommand(command, arguments);

            commandInput.text = "";
            commandInput.ActivateInputField();
        }
    }

    public void MemoryCommandUp() {
        if (commandMemory.Count > 0) {
            memoryIndex++;

            if (memoryIndex >= commandMemory.Count)
                memoryIndex = 0;

            commandInput.text = commandMemory[memoryIndex];
            commandInput.caretPosition = commandInput.text.Length;
        }
    }

    public void MemoryCommandDown() {
        if (commandMemory.Count > 0) {
            memoryIndex--;

            if (memoryIndex < 0)
                memoryIndex = commandMemory.Count - 1;

            commandInput.text = commandMemory[memoryIndex];
            commandInput.caretPosition = commandInput.text.Length;
        }
    }

    public bool Enabled {
        get { return gameObject.activeSelf; }
        set {
            gameObject.SetActive(value);
            if (value) {
                if (commandInput != null) {
                    commandInput.text = "";
                    commandInput.ActivateInputField();
                }
            }
        }
    }
}
