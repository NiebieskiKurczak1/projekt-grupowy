using System.Collections.Generic;
using UnityEngine;

public class playerWeaponSystem : MonoBehaviour
{
    [SerializeField] public List<weaponScriptableObject> weapons;
    [SerializeField] public List<weaponScriptableObject> weaponSlots;

    private InputMaster controls;

    public int selectedSlot;
    private int previousSelectedSlot;
    public bool showWeapon;
    private bool hasWeaponShowed;

    private GameObject createdWeaponObject;
    private GameObject weaponPlace;

    private void Start() {
        controls = new InputMaster();
        controls.Enable();

        controls.WeaponSystem.Slot1.performed += ctx => selectedSlot = 1;
        controls.WeaponSystem.Slot2.performed += ctx => selectedSlot = 2;
        controls.WeaponSystem.Slot3.performed += ctx => selectedSlot = 3;
        controls.WeaponSystem.showWeapon.performed += ctx => showWeapon = !showWeapon;

        weaponSlots.Insert(0, weapons.Find(i => i.name == "Bro� 1"));
        weaponSlots.Insert(1, weapons.Find(i => i.name == "Bro� 2"));
        weaponSlots.Insert(2, weapons.Find(i => i.name == "Bro� 3"));

        weaponPlace = GameObject.Find("weaponPlace").gameObject;

        selectedSlot = 1;
    }

    private void Update() {
        if (showWeapon) {
            if (!previousSelectedSlot.Equals(selectedSlot)) {
                Destroy(createdWeaponObject);
                hasWeaponShowed = false;
            }

            if (!hasWeaponShowed) {
                createdWeaponObject = Instantiate(weaponSlots.Find(i => weaponSlots.IndexOf(i) == selectedSlot - 1).model);
                createdWeaponObject.transform.position = weaponPlace.transform.position;
                createdWeaponObject.transform.SetParent(weaponPlace.transform);
                hasWeaponShowed = true;
            }
        } else {
            if (createdWeaponObject != null) {
                Destroy(createdWeaponObject);
                hasWeaponShowed = false;
            }
        }
    }
}
